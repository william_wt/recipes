const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const createToken = (user, secret, expiresIn) => {
    const { username, email } = user;
    return jwt.sign({username, email}, secret, {expiresIn})
}

exports.resolvers = {
    Query: {
        getAllRecipes: async (root, args,  { Recipe } ) => {
            const allRecipes = await Recipe.find();
            return allRecipes;
        }
    },

    Mutation: {
        addRecipe: async (
            root, 
            { 
                name, 
                description, 
                category, 
                instructions, 
                username
            }, { Recipe }) => 
            {
                const newRecipe = await new Recipe(
                    {
                        _id : new mongoose.Types.ObjectId(),
                        name,
                        description,
                        category,
                        instructions,
                        username
                    }
                ).save();
            return newRecipe;
        },

        signupUser: async (
            root, 
            {
                username, 
                email, 
                password
            }, { User }) => {
            const user = await User.findOne({ username });
            if (user) {
                throw new Error('User already exists');
            }
            const newUser = await new User({
                _id : new mongoose.Types.ObjectId(),
                username,
                email,
                password,
                joinDate : new Date(Date.now()).toLocaleString()
            }).save();
            return { token: createToken(newUser, process.env.SECRET, '2hr') }
        },

        signinUser: async (
            root,
            {
                username,
                password
            }, { User }) => {
            
            const user = await User.findOne({ username });
            if (!user) {
                throw new Error('User not found');
            }
            const isValidPassword = await bcrypt.compare(password, user.password);
            if (!isValidPassword) {
                throw new Error('Invalid Password');
            }
            return { token: createToken(user, process.env.SECRET, '2hr') }
        }
    }
};